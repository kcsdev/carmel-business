<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'carmel_wp1');

/** MySQL database username */
define('DB_USER', 'carmel_wp1');

/** MySQL database password */
define('DB_PASSWORD', 'E*DVjcvn~w^1^])(xA.13^@4');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'O1wJkB6VkXKPBo5GBBRy8SoSYw96tCepLvt6tqGRCyZHuvaYuLe9IXD5go96P9Wr');
define('SECURE_AUTH_KEY',  'FTtDtxyjBuFey2bzjhcc5jKIfnOuQwbUsuEVOVCdfKaHfmDKxSqi3rqGNfW3Db6x');
define('LOGGED_IN_KEY',    'ZCjg2ZLASbMdHQbEDtxE8RXifGNnBehj8BpAFKKwJ66qMJmXaAX0U8mrtJvlbtAe');
define('NONCE_KEY',        'dahjAXtgc9CxE2fyVMFsgSOEKFgPK0chq6NcekzPnWcSLSG8BwbfUmb1kaSWAqow');
define('AUTH_SALT',        'QwYZE5CgrmjjT5ekX9aL4N7LHfBAJFtMDgKnqJDWLOYcNtweOhMLYTycmJoimwUf');
define('SECURE_AUTH_SALT', 'lkYkrKqEeZhJSDPQdsNxp02B9BuuXhzR5Hh3j5gJHl0FNFqRs4OKGlnFGjMWHEof');
define('LOGGED_IN_SALT',   'h6wBbn9MICDZex6gm6Fp1qO6YwoFDBsmmOzRA3Bx8WXpmJnCLabfJm9nb8XMGvvP');
define('NONCE_SALT',       'EEv4sv0ONB7yADbICdLhCmAGPow21lMUlMiWDn0zoVvivD34LZpR7W7X0CwRBwVZ');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
